<!DOCTYPE html>
<html lang="en">
<head>

    <style>
        td {
            height: 20px;
            width: 70px;
            text-align: center;
        }
    </style>

</head>

<body>

    <table border="1">
        <tr>
            <th colspan="4">Person Information</th>
        </tr>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>ID</th>
            <th>Blood Group</th>
        </tr>
        <tr>
            <td><?php echo $_POST['fn'] ?></td>
            <td><?php echo $_POST['ln'] ?></td>
            <td><?php echo $_POST['id'] ?></td>
            <td><?php echo $_POST['bg'] ?></td>
        </tr>

    </table>

</body>
</html>